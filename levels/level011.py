
import k
from Part import *

def init():
    k.sound.loadTheme('industry')
    k.sound.music()
    k.world.setBackground('level-11')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
        
    k.particles.add(Particle({'pos': (cx, cy), 'color': 'white'}))    
        
    # chains
    num = k.config.stage * 6
    k.particles.chainCircle((cx, cy), 'blue', num, 200)
            
    # anchors
    anchor = [(cx, cy-100),
              (cx, cy+100),
              (cx+100, cy),
              (cx-100, cy)]
    for i in range(len(anchor)):
        n = i < 2 and (k.config.stage == 3 and 3 or 1) or min(2, k.config.stage)
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'blue', 'maxLinks': n}))
        
    # simple player
    k.player.setPos((cx+200,cy+150))
