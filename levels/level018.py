
import k
from Part import *

def init():
    k.sound.loadTheme('water')
    k.sound.music()
    k.world.setBackground('level-18')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    k.particles.add(Particle({'pos': (cx, cy), 'color': 'orange'}))

    xd = k.config.stage == 2 and 50
    rd = k.config.stage == 2 and 80
    
    # chains
    k.particles.chainCircle((cx, cy), 'white', 5, 170,   -math.pi/2)
    k.particles.chainCircle((cx, cy), 'blue',  5, 170+rd, math.pi/2)

    if k.config.stage > 1:
        k.particles.chainCircle((cx, cy), 'pink',   5, 250, -math.pi/2)
    if k.config.stage > 2:
        k.particles.chainCircle((cx, cy), 'orange', 5, 250,  math.pi/2)
        
    # anchors
    anchor = [(cx+xd-50, cy-100), (cx+xd-50, cy+100)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'white', 'maxLinks': 1}))

    anchor = [(cx+xd+50, cy-100), (cx+xd+50, cy+100)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'blue', 'maxLinks': 1}))
        
    if k.config.stage > 1:
        anchor = [(cx+xd-150, cy-100), (cx+xd-150, cy+100)]
        for i in range(len(anchor)):
            k.particles.add(Anchor({'pos':anchor[i], 'color': 'pink', 'maxLinks': 1}))
    
    if k.config.stage > 2:
        anchor = [(cx+150, cy-100), (cx+150, cy+100)]
        for i in range(len(anchor)):
            k.particles.add(Anchor({'pos':anchor[i], 'color': 'orange', 'maxLinks': 1}))
        
    # simple player
    k.player.setPos((cx+100,cy))
