
import k
from Part import *

def init():
    k.sound.loadTheme('water')
    k.sound.music()
    k.world.setBackground('level-25')
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height

    # particles
    num = [2,4,4][k.config.stage-1]
    k.particles.ballCircle((cx, cy), 'orange', num, h*0.32)
    
    # chains
    num = [8, 16, 16][k.config.stage-1]
    k.particles.chainCircle((cx, cy), 'white', num, h*0.42)

    num = [4, 4, 12][k.config.stage-1]
    k.particles.chainCircle((cx, cy), 'blue', num, h*0.32, -math.pi/2-math.pi/4)
        
    # anchors
    d = 120
    anchor = [(cx, cy-d),
              (cx+d, cy),
              (cx, cy+d),
              (cx-d, cy),]
    for i in range(len(anchor)):
        num = k.config.stage == 1 and 1 or 2
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'white', 'maxLinks': num}))

    d = 50
    anchor = [(cx, cy),
              (cx-d, cy-d),
              (cx+d, cy+d),
              (cx-d, cy+d), 
              (cx+d, cy-d),
              ]
    num_anchor = [3, 5, 5][k.config.stage-1]
    for i in range(num_anchor):
        num = i == 0 and (min(4, k.config.stage*2)) or [1,1,3][k.config.stage-1]
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'blue', 'maxLinks': num}))
        
    # simple player
    k.player.setPos((cx+w*0.23,cy+h*0.38))
