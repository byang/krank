#
# levels/__init__.py

import k
from Krank import *
from Level import *

numLevels = 0
numBonusLevels = 0

for file in os.listdir(os.path.dirname(__file__)):
    if os.path.isfile(os.path.join(os.path.dirname(__file__), file)) and file[:5] == 'level' and file[-3:] == '.py':
        numLevels += 1
    if os.path.isfile(os.path.join(os.path.dirname(__file__), file)) and file[:5] == 'bonus' and file[-3:] == '.py':
        numBonusLevels += 1
