import k, levels
from Krank  import *
from Part import *

def init():
    k.sound.loadTheme('menu')
    k.sound.music('industry.ogg') #k.sound.music('space.ogg')
    k.world.setBackground('menu-play')
    
    # player
    k.player.setPos(k.world.rect.center)
    k.player.setTailNum(2)
    
    c, h = k.world.rect.centerx, k.world.rect.height
        
    k.particles.add(Switch({ 'text': 'Start',
                             'action': 'k.level.startExit()',
                             'pos': vector((1*k.world.rect.width/4, k.world.rect.height*5/12))}))

    log(k.config.lastSolvedLevel())
    k.particles.add(Switch({ 'text': 'Continue', 
                             'align': 'right',
                             'action': 'k.level.startExit(%d)' % k.config.lastSolvedLevel(),
                             'pos': vector((3*k.world.rect.width/4, k.world.rect.height*5/12))}))

    k.particles.add(Switch({ 'text': 'Select Level',
                             'action': 'k.level.menuExit("menu_levels")',
                             'align': 'top',
                             'pos': vector((k.world.rect.width/2, k.world.rect.height*0.65))}))

    k.particles.add(Switch({ 'text': 'Back',
                             'align': 'bottom',
                             'action': 'k.level.menuExit()',
                             'size': 'small', 
                             'pos': vector((c, h*7/8))}))
    
