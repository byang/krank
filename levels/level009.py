
import k
from Part import *

def init():
    k.sound.loadTheme('water')
    k.sound.music()
    k.world.setBackground('level-09')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # chains
    chains = [(cx, h*1/5),
              (cx, h*4/5),
              (w*1/4, h*1/4),
              (w*1/4, h*3/4),              
              (w*3/4, h*1/4),
              (w*3/4, h*3/4)]
    
    for i in range(len(chains)):
        k.particles.add(Chain({'pos': chains[i], 'color': 'orange'}))
        
    # anchors
    d = 140
    anchor = [(cx-d, cy), (cx+d, cy)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'orange', 'maxLinks': k.config.stage>=3 and 2 or 1}))
        
    if k.config.stage >= 2:
        k.particles.add(Anchor({'pos': (cx, cy), 'color': 'orange', 'maxLinks': 2}))
        k.particles.ballCircle((cx, cy), 'blue', k.config.stage*2, 70)
        if k.config.stage >= 3:
            k.particles.chainCircle((cx, cy), 'orange', 6, 120, -math.pi/2)
    else:
        k.particles.add(Stone({'pos': (cx, cy), 'color': 'orange'}))
        
    # simple player
    k.player.setPos((cx, h*2/3))
