
import k
from Part import *

def init():
    k.sound.loadTheme('water')
    k.sound.music()
    k.world.setBackground('bonus-01')
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
        
    xn, yn = 9,7
    
    for x in range(xn):
        for y in range(yn):
            if x <> xn/2 and y <> yn/2:
                pos = ((x+1.0)*w/(xn+1.0), (y+1.0)*h/(yn+1.0))
                k.particles.add(Chain({'pos': pos, 'color': 'blue'}))
                
    # simple player
    k.player.setPos((cx, cy))
