import k
from levels import *
from Part import *

def init():
    k.sound.loadTheme('industry')
    k.sound.music()
    k.world.setBackground('level-13')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    xd = 150
    # 6 white particles
    num = min(6, 2+k.config.stage)    
    k.particles.ballCircle((cx-xd, cy), 'blue', num, 120)
    k.particles.ballCircle((cx+xd, cy), 'white', num, 120, -math.pi)
    
    # 2 magnets 
    k.particles.add (Magnet({'pos': (cx-xd, cy), 'color': 'white', 'num': num}))
    k.particles.add (Magnet({'pos': (cx+xd, cy), 'color': 'blue', 'num': num}))

    # chains
    k.particles.chainCircle((cx, cy), 'white', 4, 100, -math.pi/4)
    
    # anchors
    anchor = [(cx-80, cy),
              (cx+80, cy)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'white', 'maxLinks': 1}))
        
    # simple player
    k.player.setPos((w/2, cy-3*h/16))

