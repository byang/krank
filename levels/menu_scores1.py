import k
from levels import *
from Part import *
from Effect import *
from Tools import *

def init():
    
    stage = 1
    numLevels = k.config.numAvailableLevels(stage)
    if numLevels > 20:
        k.world.setBackground('menu-scores1')
    else:
        k.world.setBackground('menu-scores1')
    # simple player
    k.player.setPos(k.world.rect.center)
    k.player.setTailNum(2)
    
    c, w, h = k.world.rect.centerx, k.world.rect.width, k.world.rect.height
    
    drawText("Easy Scores", (c, h*2/30), color=(0,0,0))

    for level in range(1, numLevels+1):
        level_index = level-1
        time = k.config.bestTimeString(level, stage)
        y = numLevels>20 and h*(level_index%15+5)/25 or h*(level_index+5)/30
        x = numLevels>20 and (level_index < 15 and w*0.35 or w*0.62) or c
        lo = numLevels>20 and w/12 or w/6
        level_txt = numLevels>20 and ("%2d" % level) or "Level %2d" % level
        drawText(level_txt, (x-lo, y), size='normal', color=(0,0,0), align='left')
        drawText(time, (x, y), size='normal', color=(0,0,0), align='center')
        drawText(k.config.bestScoreString(level, stage), (x+w/8, y), size='normal', color=(0,0,0), align='right')
        
    k.particles.add(Switch({ 'text': 'Back',
                             'align': 'bottom',
                             'action': 'k.level.menuExit("menu_scores")',
                             'size': 'small', 
                             'pos': pos(c, h*7/8)}))
