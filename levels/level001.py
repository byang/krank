
import k
from Part import *

def init():
    k.sound.loadTheme('space')
    k.sound.music()
    k.world.setBackground('level-01')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # particles
    parts = [(w*1/3, cy), (w*2/3, cy)]
        
    if k.config.stage == 2:
        parts.extend([(cx, h*1/3), (cx, h*2/3)]) 
    elif k.config.stage == 3:
        parts.extend([(cx-w*1/12, h*1/3), (cx-w*1/12, h*2/3), (cx+w*1/12, h*1/3), (cx+w*1/12, h*2/3)]) 
    
    for i in range(len(parts)):
        k.particles.add(Particle({'pos': parts[i], 'color': 'white'}))

    # stones
    stones = []
    xd, yd = 80, 60 
    if k.config.stage >= 2:
        stones.extend([(cx, cy-yd), (cx, cy+yd), (cx-xd, cy), (cx+xd, cy)])
    if k.config.stage >= 3:
        stones.extend([(cx-xd, cy-yd), (cx+xd, cy-yd), (cx-xd, cy+yd), (cx+xd, cy+yd)])

    for i in range(len(stones)):
        k.particles.add(Stone({'pos': stones[i], 'color': 'white'}))
    
    # magnet 
    n = min(6, k.config.stage*2)
    k.particles.add (Magnet({'pos': (cx, cy), 'color': 'white', 'num': n}))
    
    # simple player
    k.player.setPos((cx, cy+h*1/4))
