
import k
from levels import *
from Part import *

def init():
    k.sound.loadTheme('summer')
    k.sound.music()
    k.world.setBackground('level-22')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # particles
    parts = [(5*w/12,  4*h/12),
             (8*w/12,  2*h/12),
             (7*w/12,  4*h/12),
             (3*w/12,  6*h/12),
             (9*w/12,  6*h/12),
             (5*w/12,  8*h/12),
             (7*w/12,  8*h/12),
             (4*w/12, 10*h/12)]
    
    for i in range(8):
        k.particles.add(Particle({'pos': parts[i], 'color': 'orange'}))
    
    parts = [(6*w/12,  2*h/12),
             (4*w/12,  2*h/12),
             (3*w/12,  4*h/12),
             (9*w/12,  4*h/12),
             (9*w/12,  8*h/12),
             (3*w/12,  8*h/12),
             (6*w/12, 10*h/12),
             (8*w/12, 10*h/12)]
    
    for i in range(8):
        k.particles.add(Particle({'pos': parts[i], 'color': 'blue'}))

    # magnets 
    num = k.config.stage * 2
    k.particles.add (Magnet({'pos': (  w/4,   h/4), 'color': 'blue',   'num': num}))
    k.particles.add (Magnet({'pos': (3*w/4,   h/4), 'color': 'orange', 'num': num}))
    k.particles.add (Magnet({'pos': (  w/4, 3*h/4), 'color': 'orange', 'num': num}))    
    k.particles.add (Magnet({'pos': (3*w/4, 3*h/4), 'color': 'blue',   'num': num}))
    k.particles.add (Magnet({'pos': (  w/3,   cy), 'color': 'orange',  'num': num+1}))    
    k.particles.add (Magnet({'pos': (2*w/3,   cy), 'color': 'blue',    'num': num+1}))
    
    # simple player
    k.player.setPos((w/2, 5*h/12))

