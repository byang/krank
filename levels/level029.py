
import k
from Part import *

def init():
    k.sound.loadTheme('space')
    k.sound.music()
    k.world.setBackground('level-29')
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height

    k.particles.add(Particle({'pos': (cx, cy), 'color': 'blue'}))
    
    # chains
    num = [6,12,16][k.config.stage-1]
    k.particles.chainCircle((cx, cy), 'orange', num, h*0.42)

    num = [4,8,8][k.config.stage-1]
    k.particles.chainCircle((cx, cy), 'pink', num, h*0.32, -math.pi/8)
        
    # anchors
    d = 120
    anchor = [(cx+d, cy),
              (cx, cy-d),
              (cx, cy+d),
              (cx-d, cy),
              (cx+d, cy+d),
              (cx-d, cy-d),
              (cx-d, cy+d),
              (cx+d, cy-d),]
    num_anchor = k.config.stage == 3 and 8 or 4
    for i in range(num_anchor):
        num = min(2, k.config.stage)
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'orange', 'maxLinks': num}))

    d = 50
    anchor = [(cx-d, cy-d),
              (cx+d, cy-d),
              (cx+d, cy+d),
              (cx-d, cy+d), ]
    for i in range(len(anchor)):
        num = min(2, k.config.stage)
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'pink', 'maxLinks': num}))
        
    # simple player
    k.player.setPos((w*2/3,cy+h*0.42))
