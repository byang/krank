import k
from levels import *
from Part import *
from Effect import *

def init():
    k.sound.loadTheme('menu')
    k.sound.music('industry.ogg')
    k.world.setBackground('menu-screen')
    k.world.darken(60)
    # simple player
    k.player.setPos(k.world.rect.center)
    k.player.setTailNum(2)
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    drawText('Screen', (cx, h*2.0/24.0), valign='center')
    k.particles.add(Switch({ 'text': 'Back',
                             'align': 'bottom',
                             'action': 'k.config.save() or k.level.menuExit()',
                             'size': 'small', 
                             'pos': pos(cx-cx/3, h*7/8)}))
    
    k.particles.add(Switch({ 'text': 'Fullscreen',
                             'align': 'bottom',
                             'action': 'k.world.toggleFullscreen()',
                             'size': 'small', 
                             'pos': pos(cx+cx/3, h*7/8)}))
    modes = pygame.display.list_modes(32, pygame.FULLSCREEN)
    modes = [m for m in modes if (m[1] >= 600)]
    set = sets.Set()
    for m in modes: set.add(m)
    modes = list(set)
    modes.sort(reverse=True)
    for i in range(len(modes)):
        m = modes[i]
        active = m==k.world.rect.size
        dirvec = vector.withAngle(-(i+1)*2*math.pi/(len(modes)+1)+math.pi/2, h*5/12)
        dirvec.y *= 0.6
        k.particles.add(Switch({ 'text': "%dx%d" % m,
                                 'size': 'small',
                                 'align': 'bottom',
                                 'action': 'k.world.setScreen((%d, %d))' % m,
                                 'color': active and 'orange' or 'white', 
                                 'pos': vector(k.world.rect.center)+dirvec}))
    
