
import k
from Part import *

def init():
    k.sound.loadTheme('summer')
    k.sound.music()
    k.world.setBackground('level-07')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # particles
    parts = [(  w/4,   h/4),
             (  w/2,   h/4),
             (3*w/4,   h/4),
             (  w/4, 3*h/4),
             (  w/2, 3*h/4),
             (3*w/4, 3*h/4)]
    
    for i in range(6):
        k.particles.add(Particle({'pos': parts[i], 'color': 'orange'}))
    
    # magnet 
    n = min(6, 3+k.config.stage)
    k.particles.add (Magnet({'pos': (w/2, h/2), 'color': 'orange', 'num': n}))

    # stones
    if k.config.stage >= 2:
        num = (k.config.stage-1)*5
        k.particles.stoneCircle((cx, cy), 'orange', num, 90, -math.pi/2)
        
    # simple player
    k.player.setPos((cx, cy-130))

