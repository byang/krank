
import k
from Part import *

def init():
    k.sound.loadTheme('industry')
    k.sound.music()
    k.world.setBackground('level-20')
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height

    # particles
    if k.config.stage > 1:
        parts = [ (cx-w*2/6,  cy+h*2/6),
                  (cx-w*2/6,  cy-h*2/6),
                  (cx+w*2/6,  cy+h*2/6),
                  (cx+w*2/6,  cy-h*2/6)]
    else:
        parts = [(cx-(w*0.31+120),  cy), (cx+(w*0.31+120),  cy)]
    
    for part in parts:
        k.particles.add(Particle({'pos': part, 'color': 'orange'}))
    
    # magnet 
    if k.config.stage < 3:
        k.particles.add (Magnet({'pos': (cx, cy), 'color': 'orange', 'num': k.config.stage*2}))

    # chains
    chains = [(cx,       cy-h*0.37),
              (cx,       cy+h*0.37),
              (cx-w*0.16, cy-h*1/4),
              (cx-w*0.16, cy+h*1/4),              
              (cx-w*0.22, cy-h*1/8),
              (cx-w*0.22, cy+h*1/8),              
              (cx-w*0.31, cy),
              (cx+w*0.16, cy-h*1/4),
              (cx+w*0.16, cy+h*1/4),
              (cx+w*0.22, cy-h*1/8),
              (cx+w*0.22, cy+h*1/8),
              (cx+w*0.31, cy),]
    
    for i in range(len(chains)):
        k.particles.add(Chain({'pos': chains[i], 'color': 'blue'}))
    if k.config.stage > 1:
        for i in range(len(chains)):
            o = i > 1 and pos(i > 6 and 60 or -60, 0) or pos(0, i and -60 or 60)
            k.particles.add(Chain({'pos': vector(chains[i])+o, 'color': 'blue'}))
    if k.config.stage > 2:
        for i in range(len(chains)):
            o = i > 1 and pos(i > 6 and 120 or -120, 0) or pos(0, i and -120 or 120)
            k.particles.add(Chain({'pos': vector(chains[i])+o, 'color': 'blue'}))
                
    d = 80
    anchor = [(cx-d, cy-d),
              (cx-d, cy+d), 
              (cx+d, cy-d),
              (cx+d, cy+d),
              (cx+2.4*d, cy),
              (cx-2.4*d, cy),
              (cx, cy)]
    num_anchor = [4, 6, 7][k.config.stage-1]
    links = [(2, 2 , 2, 2), (3, 3, 3, 3, 2, 2), (4, 4, 4, 4, 2, 2, 4)][k.config.stage-1]
    for i in range(num_anchor):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'blue', 'maxLinks': links[i]}))
        
    # simple player
    k.player.setPos((cx+w*0.16,cy+h*0.37))
