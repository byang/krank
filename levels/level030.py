
import k
from Part import *

def init():
    k.sound.loadTheme('summer')
    k.sound.music()
    k.world.setBackground('level-30')
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height

    # particle
    pos = k.config.stage < 3 and (cx, cy) or (cx-w*0.42, cy)
    k.particles.add(Particle({'pos': pos, 'color': 'blue'}))
    
    # chains
    chains = [(cx+w*3/8,       cy-h*1/8),
              (cx+w*3/8,       cy+h*1/8),
              (cx-w*3/8,       cy-h*1/8),
              (cx-w*3/8,       cy+h*1/8),
              (cx-w*3/8, cy-h*1/4),
              (cx+w*3/8, cy-h*1/4),
              (cx-w*3/8, cy+h*1/4),
              (cx+w*3/8, cy+h*1/4),
              (cx-w*1/4, cy-h*1/4),
              (cx-w*1/4, cy+h*1/4),              
              (cx+w*1/4, cy-h*1/4),
              (cx+w*1/4, cy+h*1/4),
              (cx-w*1/8, cy-h*1/4),
              (cx-w*1/8, cy+h*1/4),              
              (cx+w*1/8, cy-h*1/4),
              (cx+w*1/8, cy+h*1/4),
              (cx-w*1/4, cy-h*1/8),
              (cx-w*1/4, cy+h*1/8),              
              (cx+w*1/4, cy-h*1/8),
              (cx+w*1/4, cy+h*1/8),
              (cx-w*1/4, cy),
              (cx+w*1/4, cy),
              (cx-w*2/6, cy),
              (cx+w*2/6, cy),
              (cx-w*2/6,  cy+h*2/6),
              (cx-w*2/6,  cy-h*2/6),
              (cx+w*2/6,  cy+h*2/6),
              (cx+w*2/6,  cy-h*2/6),
              (cx+w*1/16,  cy-h*2/6),
              (cx+w*1/16,  cy+h*2/6),
              (cx-w*1/16,  cy-h*2/6),
              (cx-w*1/16,  cy+h*2/6),
              (cx+w*3/16,  cy-h*2/6),
              (cx+w*3/16,  cy+h*2/6),
              (cx-w*3/16,  cy-h*2/6),
              (cx-w*3/16,  cy+h*2/6),
]
    
    num_chains = [20, 28, len(chains)][k.config.stage-1]
    for i in range(num_chains):
        k.particles.add(Chain({'pos': chains[i], 'color': 'orange'}))
        
    d = 80
    anchor = [(cx-d, cy-d),
              (cx-d, cy+d), 
              (cx+d, cy-d),
              (cx+d, cy+d),
              (cx, cy-2*d),
              (cx, cy+2*d), 
              (cx+2*d, cy),
              (cx-2*d, cy),
              (cx, cy)]
    num_anchor = [6, 8, 9][k.config.stage-1]
    for i in range(num_anchor):
        num = (i < 4) and (k.config.stage==2 and 4 or 3) or (i == 8 and 8 or (k.config.stage==3 and 3 or 2))
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'orange', 'maxLinks': num}))
        
    # simple player
    k.player.setPos((w*2/3,cy+h*0.42))
