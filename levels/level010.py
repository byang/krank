
import k
from levels import *
from Part import *
from Effect import *

def init():
    k.sound.loadTheme('space')
    k.sound.music()
    k.world.setBackground('level-10')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # particles
    parts = [(cx,   h*3/16),
             (cx,   h*13/16)]
    
    if k.config.stage >= 2:
        parts.extend([(w/4,   cy), (w*3/4, cy)])
    
    if k.config.stage >= 3:
        parts.extend([(w/8, cy), (w*7/8, cy)]) 
        
    log(parts)
    
    for i in range(len(parts)):
        k.particles.add(Particle({'pos': parts[i], 'color': 'white'}))    
    
    # magnets 
    n = min(k.config.stage*2, 6)
    k.particles.add (Magnet({'pos': (cx,     h*1/3), 'color': 'white', 'num': n}))
    k.particles.add (Magnet({'pos': (cx,     h*2/3), 'color': 'white', 'num': n}))
    k.particles.add (Magnet({'pos': (w*9/24,  cy),   'color': 'white', 'num': n}))
    k.particles.add (Magnet({'pos': (w*15/24, cy),   'color': 'white', 'num': n}))

    # simple player
    k.player.setPos(k.world.rect.center)
