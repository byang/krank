
import k
from levels import *
from Part import *

def init():
    k.sound.loadTheme('summer')
    k.sound.music()
    k.world.setBackground('level-19')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # particles
    parts = [(5*w/12, 4*h/12),
             (7*w/12, 4*h/12),
             (4*w/12, 6*h/12),
             (8*w/12, 6*h/12),
             (5*w/12, 8*h/12),
             (7*w/12, 8*h/12)]
    
    for i in range(6):
        k.particles.add(Particle({'pos': parts[i], 'color': 'orange'}))
    
    parts = [(6*w/12,  2*h/12),
             (3*w/12,  4*h/12),
             (9*w/12,  4*h/12),
             (9*w/12,  8*h/12),
             (3*w/12,  8*h/12),
             (6*w/12, 10*h/12)]
    
    for i in range(6):
        k.particles.add(Particle({'pos': parts[i], 'color': 'pink'}))

    parts = [( 4*w/12,  2*h/12),
             ( 8*w/12,  2*h/12),
             ( 2*w/12,  6*h/12),
             (10*w/12,  6*h/12),
             ( 4*w/12, 10*h/12),
             ( 8*w/12, 10*h/12)]
    
    for i in range(6):
        k.particles.add(Particle({'pos': parts[i], 'color': 'white'}))

    # 4 magnets 
    num = k.config.stage+3
    k.particles.add (Magnet({'pos': ( 5*w/24,  5*h/24), 'color': 'white',  'num': num}))
    k.particles.add (Magnet({'pos': (19*w/24,  5*h/24), 'color': 'orange', 'num': num}))
    k.particles.add (Magnet({'pos': ( 5*w/24, 19*h/24), 'color': 'orange', 'num': num}))    
    k.particles.add (Magnet({'pos': (19*w/24, 19*h/24), 'color': 'white',  'num': num}))
    k.particles.add (Magnet({'pos': (   w/2,     h/2),  'color': 'pink',   'num': num}))

    # simple player
    k.player.setPos((w/2, 5*h/12))

