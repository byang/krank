
import k
from Part import *

def init():
    k.sound.loadTheme('water')
    k.sound.music()
    k.world.setBackground('level-05')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    xd = 170
    # particles
    if k.config.stage == 1:
        parts = [( cx-w*0.2, cy-h*0.2 ),
                 ( cx+w*0.2, cy+h*0.2 ),
                 ( cx+w*0.2, cy-h*0.2 ),
                 ( cx-w*0.2, cy+h*0.2 )]
        
        for i in range(len(parts)):
            k.particles.add(Particle({'pos': parts[i], 'color': i < 2 and 'blue' or 'white'}))
    else:
    	if k.config.stage == 2:
    	    num = 4
    	else:
            num = (k.config.stage-1)*3
        k.particles.ballCircle((cx-xd, cy), 'white',  num, 150)
        k.particles.ballCircle((cx+xd, cy), 'blue',   num, 150)        
    
    # magnets 
    n = min(6, k.config.stage*2)
    k.particles.add (Magnet({'pos': (cx-xd, cy), 'color': 'white', 'num': n}))
    k.particles.add (Magnet({'pos': (cx+xd, cy), 'color': 'blue',  'num': n}))

    # stones
    if k.config.stage >= 2:
        num = (k.config.stage-1)*3
        k.particles.stoneCircle((cx-xd, cy), 'blue',  num, 70+(k.config.stage-2)*30)
        k.particles.stoneCircle((cx+xd, cy), 'white', num, 70+(k.config.stage-2)*30)        
        
    # simple player
    k.player.setPos((cx, cy+(k.config.stage > 1 and h/4)))
