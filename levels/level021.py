
import k
from Part import *

def init():
    k.sound.loadTheme('space')
    k.sound.music()
    k.world.setBackground('level-21')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    k.particles.add(Particle({'pos': (cx, cy), 'color': 'white'}))
    
    if k.config.stage == 1:
        d1, d2 = 140, 220
    elif k.config.stage == 2:
        d1, d2 = 170, 270
    else:
        d1, d2 = 170, 320
        
    d2y = min(k.world.rect.height/2 - 20, d2)
    d3, d3y = (d1+d2)/2, (d1+d2y)/2
    
    num_chains = k.config.stage+2
    
    # chains
    chains = [(cx+d1, cy+d1),
              (cx+d1, cy+d2y),
              (cx+d2, cy+d1),
              (cx+d2, cy+d2y),
              (cx+d3, cy+d3y)]
    
    for i in range(num_chains):
        k.particles.add(Chain({'pos': chains[i], 'color': 'white'}))

    # chains
    chains = [(cx-d1, cy+d1),
              (cx-d1, cy+d2y),
              (cx-d2, cy+d1),
              (cx-d2, cy+d2y),
              (cx-d3, cy+d3y)]
    
    for i in range(num_chains):
        k.particles.add(Chain({'pos': chains[i], 'color': 'orange'}))

    # chains
    chains = [(cx+d1, cy-d1),
              (cx+d1, cy-d2y),
              (cx+d2, cy-d1),
              (cx+d2, cy-d2y),
              (cx+d3, cy-d3y)]
    
    for i in range(num_chains):
        k.particles.add(Chain({'pos': chains[i], 'color': 'blue'}))

    # chains
    chains = [(cx-d1, cy-d1),
              (cx-d1, cy-d2y),
              (cx-d2, cy-d1),
              (cx-d2, cy-d2y),
              (cx-d3, cy-d3y)]
    
    for i in range(num_chains):
        k.particles.add(Chain({'pos': chains[i], 'color': 'pink'}))
        
    # anchors
    anchor = [(cx+70, cy), (cx+d2, cy)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'orange', 'maxLinks': 1}))

    anchor = [(cx-70, cy), (cx-d2, cy)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'blue', 'maxLinks': 1}))
        
    anchor = [(cx, cy+70), (cx, cy+d2y)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'pink', 'maxLinks': 1}))
                
    anchor = [(cx, cy-70), (cx, cy-d2y)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'white', 'maxLinks': 1}))                
        
    # simple player
    k.player.setPos((cx+70,cy-70))
