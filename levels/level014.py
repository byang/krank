
import k
from levels import *
from Part import *

def init():
    k.sound.loadTheme('space')
    k.sound.music()
    k.world.setBackground('level-14')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # 18 particles
    parts = [(  w/4,    h/4),
             (3*w/4,    h/4),
             (  w/6,    h/4),
             (5*w/12,   h/4),
             (6*w/12,   h/4),
             (7*w/12,   h/4),
             (5*w/6,    h/4),
             (  w/3,    h/4),
             (2*w/3,    h/4),
             (  w/4,  3*h/4),
             (3*w/4,  3*h/4),
             (  w/6,  3*h/4),
             (5*w/12, 3*h/4),
             (6*w/12, 3*h/4),
             (7*w/12, 3*h/4),
             (5*w/6,  3*h/4),
             (  w/3,  3*h/4),
             (2*w/3,  3*h/4)]
    
    for i in range(18):
        k.particles.add(Particle({'pos': parts[i], 'color': (i%2) and 'orange' or 'blue'}))
    
    # 3 magnets 
    k.particles.add (Magnet({'pos': (1*w/3, h/2), 'color': 'orange', 'num': min(5, 2+k.config.stage)}))
    k.particles.add (Magnet({'pos': (2*w/3, h/2), 'color': 'orange', 'num': min(5, 2+k.config.stage)}))
    k.particles.add (Magnet({'pos': (w/2, h/2),   'color': 'blue', 'num': min(6, 3+k.config.stage)}))

    # simple player
    k.player.setPos((w/2, 3*h/8))
