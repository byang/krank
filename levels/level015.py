
import k
from Part import *

def init():
    k.sound.loadTheme('industry')
    k.sound.music()
    k.world.setBackground('level-15')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
   
    # particles
    k.particles.ballCircle((cx, cy), 'orange', k.config.stage * 2, 200, -math.pi/2)
        
    # chains
    if k.config.stage == 1:
        k.particles.chainCircle((cx, cy), 'orange', 4, 250)
    
        chains = [( cx-w*0.2, cy+h*0.1 ), ( cx+w*0.2, cy-h*0.1 ), ( cx+w*0.2, cy+h*0.1 ), ( cx-w*0.2, cy-h*0.1 )]
        
        for i in range(len(chains)):
            k.particles.add(Chain({'pos': chains[i], 'color': 'blue'}))
    else:
        c = k.config.stage==2 and 6 or 15
        k.particles.chainCircle((cx, cy), 'orange', c, 280)
        k.particles.chainCircle((cx, cy), 'blue',   c, 280, k.config.stage == 2 and -math.pi/2 or -math.pi/15)
    
    # magnets 
    k.particles.add (Magnet({'pos': (cx, cy), 'color': 'orange',  'num': min(6, k.config.stage*2)}))
        
    # anchors
    yd = 60
    anchor = [(cx-70, cy-yd), (cx-70, cy+yd), (cx-140, cy), (cx-220, cy)]
    for i in range(min(len(anchor), k.config.stage+1)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'blue', 'maxLinks': k.config.stage}))

    anchor = [(cx+70, cy-yd), (cx+70, cy+yd), (cx+140, cy), (cx+220, cy)]
    for i in range(min(len(anchor), k.config.stage+1)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'orange', 'maxLinks': k.config.stage}))
        
    # player
    k.player.setPos((cx, cy+145))
