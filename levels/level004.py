
import k
from Part import *

def init():
    k.sound.loadTheme('space')
    k.sound.music()
    k.world.setBackground('level-04')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # chains
    chains = [(w/2, h*1/5),
              (w/2, h*4/5),
              (w*1/4, h*1/4),
              (w*1/4, h*3/4),              
              (w*3/4, h*1/4),
              (w*3/4, h*3/4)]
    
    for i in range(len(chains)):
        k.particles.add(Chain({'pos': chains[i], 'color': 'white'}))
        
    # anchors
    dy = 100
    anchor = [(cx, cy-dy), (cx, cy+dy)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'white', 'maxLinks': 1}))

    # stones
    if k.config.stage >= 2:
        num = k.config.stage == 2 and 10 or 16
        for i in range(num):
            pos = (cx-num*14+(i+0.5)*28, cy)
            k.particles.add(Stone({'pos': pos, 'color': 'white'}))
                
    # simple player
    k.player.setPos((cx+(k.config.stage>1 and dy), cy++(k.config.stage>1 and dy)))
