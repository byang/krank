import k
from Krank  import *
from Part import *

def init():
    k.sound.loadTheme('menu')
    k.sound.music('space.ogg')
    k.world.setBackground('menu')
    k.world.darken(70)
    
    # player
    k.player.setPos(k.world.rect.center)
    k.player.setTailNum(2)
    
    k.particles.add(Switch({ 'text': 'Play',
                             'action': 'k.config.numSolvedLevels() and k.level.menuExit("menu_play") or k.level.startExit()',
                             'pos': pos(1*k.world.rect.width/4, 1*k.world.rect.height/3)}))

    k.particles.add(Switch({ 'text': 'Scores', 
                             'align': 'right',
                             'action': 'k.level.menuExit("menu_scores")',
                             'pos': pos(3*k.world.rect.width/4, 1*k.world.rect.height/3)}))

    k.particles.add(Switch({ 'text': 'Sound',
                             'action': 'k.level.menuExit("menu_sound")',
                             'pos': pos(1*k.world.rect.width/4, 4*k.world.rect.height/7)}))

    k.particles.add(Switch({ 'text': 'Screen', 
                             'align': 'right',
                             'action': 'k.level.menuExit("menu_screen")',
                             'pos': pos(3*k.world.rect.width/4, 4*k.world.rect.height/7)}))
    
    k.particles.add(Switch({ 'text': 'Credits', 
                             'align': 'bottom',
                             'action': 'k.level.menuExit("menu_credits")',
                             'pos': pos(k.world.rect.width/2, 3*k.world.rect.height/4)}))
