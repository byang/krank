
import k
from Part import *

def init():
    k.sound.loadTheme('space')
    k.sound.music()
    k.world.setBackground('level-26')
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height

    # particles
    parts  = [(cx-w*2/6,  cy+h*2/6),
              (cx-w*2/6,  cy-h*2/6),
              (cx+w*2/6,  cy+h*2/6),
              (cx+w*2/6,  cy-h*2/6),]
    
    for part in parts:
        k.particles.add(Particle({'pos': part, 'color': 'blue'}))
    
    # chains
    chains = [(cx-w*1/4, cy-h*1/4),
              (cx-w*1/4, cy+h*1/4),              
              (cx+w*1/4, cy-h*1/4),
              (cx+w*1/4, cy+h*1/4),
              (cx-w*1/8, cy-h*1/4),
              (cx-w*1/8, cy+h*1/4),              
              (cx+w*1/8, cy-h*1/4),
              (cx+w*1/8, cy+h*1/4),
              (cx,       cy-h*2/6),
              (cx,       cy+h*2/6),
              (cx-w*1/4, cy-h*1/8),
              (cx-w*1/4, cy+h*1/8),              
              (cx+w*1/4, cy-h*1/8),
              (cx+w*1/4, cy+h*1/8),
              (cx-w*1/4, cy),
              (cx+w*1/4, cy),
              (cx-w*2/6, cy),
              (cx+w*2/6, cy),
              (cx, cy-h*1/4),
              (cx, cy+h*1/4),
              (cx+w*1/4, cy-h*2/6),
              (cx+w*1/4, cy+h*2/6),
              (cx+w*1/8, cy-h*2/6),
              (cx+w*1/8, cy+h*2/6),
              (cx-w*1/4, cy-h*2/6),
              (cx-w*1/4, cy+h*2/6),
              (cx-w*1/8, cy-h*2/6),
              (cx-w*1/8, cy+h*2/6)]
    
    num_chains = [14, 20, 28][k.config.stage-1]
    for i in range(num_chains):
        k.particles.add(Chain({'pos': chains[i], 'color': 'white'}))
        
    d = 80
    anchor = [(cx, cy),
              (cx-d, cy-d),
              (cx-d, cy+d), 
              (cx+d, cy-d),
              (cx+d, cy+d),
              (cx-2*d, cy),
              (cx+2*d, cy),]
    num_anchor = k.config.stage == 3 and 7 or 5
    for i in range(num_anchor):
        num = i == 0 and 4 or (i > 4 and 2 or [2, 3, 4][k.config.stage-1])
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'white', 'maxLinks': num}))
        
    # simple player
    k.player.setPos((w*2/3,cy+h*0.42))
