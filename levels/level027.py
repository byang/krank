
import k
from Part import *

def init():
    k.sound.loadTheme('industry')
    k.sound.music()
    k.world.setBackground('level-27')
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # particles
    parts = [ [(cx-w*2/6,  cy+h*2/6), 'blue'],
              [(cx-w*2/6,  cy-h*2/6), 'blue'],
              [(cx+w*2/6,  cy+h*2/6), 'blue'],
              [(cx+w*2/6,  cy-h*2/6), 'blue'],
              [(cx,        cy-h*2/6), 'blue'],
              [(cx,        cy+h*2/6), 'blue'],
              [(cx-w*1/4, cy-h*1/4), 'orange'],
              [(cx-w*1/4, cy+h*1/4), 'orange'],     
              [(cx+w*1/4, cy-h*1/4), 'orange'],
              [(cx+w*1/4, cy+h*1/4), 'orange'],
              [(cx-w*1/8, cy-h*1/4), 'orange'],
              [(cx-w*1/8, cy+h*1/4), 'orange'],     
              [(cx+w*1/8, cy-h*1/4), 'pink'],
              [(cx+w*1/8, cy+h*1/4), 'pink'],
              [(cx-w*1/4, cy-h*1/8), 'pink'],
              [(cx-w*1/4, cy+h*1/8), 'pink'],     
              [(cx+w*1/4, cy-h*1/8), 'pink'],
              [(cx+w*1/4, cy+h*1/8), 'pink'],
              [(cx-w*1/4, cy), 'white'],
              [(cx+w*1/4, cy), 'white'],
              [(cx-w*2/6, cy), 'white'],
              [(cx+w*2/6, cy), 'white'],
              [(cx, cy-h*1/4), 'white'],
              [(cx, cy+h*1/4), 'white'],]
    
    for i in range(len(parts)):
        k.particles.add(Particle({'pos': parts[i][0], 'color': parts[i][1]}))
        
    num = k.config.stage+3    
        
    d = 80
    magnet = [[(cx-d, cy-d), 'orange'],
              [(cx-d, cy+d), 'white'], 
              [(cx+d, cy-d), 'blue'],
              [(cx+d, cy+d), 'pink'],]
    for i in range(len(magnet)):
        k.particles.add(Magnet({'pos':magnet[i][0], 'color': magnet[i][1], 'num': num}))
        
    # simple player
    k.player.setPos((w*0.6,h*0.89))
