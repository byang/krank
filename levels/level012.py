
import k
from levels import *
from Part import *
from Effect import *

def init():
    k.sound.loadTheme('water')
    k.sound.music()
    k.world.setBackground('level-12')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # particles
    if k.config.stage == 1:
        parts = [(w/8, cy), (w*7/8, cy)]
    elif k.config.stage == 2:
        parts = [(w/4, h/4), (3*w/4, h/4), (w/4, 3*h/4), (3*w/4, 3*h/4)]
    elif k.config.stage >= 3:
        parts = [(w/4, h/4), (3*w/4, h/4), (w/4, 3*h/4), (3*w/4, 3*h/4), (w/8, cy), (w*7/8, cy)]
    
    for i in range(len(parts)):
        k.particles.add(Particle({'pos': parts[i], 'color': 'white'}))
    
    # particles
    parts = [(w/4, cy), (w*3/4, cy)]
    
    if k.config.stage >= 2:
        parts.extend([(cx, h*3/16), (cx, h*13/16)])

    if k.config.stage >= 3:
        parts.extend([(cx, h*3/32), (cx, h*29/32)])
    
    for i in range(len(parts)):
        k.particles.add(Particle({'pos': parts[i], 'color': 'blue'}))    
    
    # magnets 
    n = min(k.config.stage*2, 6)
    k.particles.add (Magnet({'pos': (w/2,     h*1/3), 'color': 'white', 'num': n}))
    k.particles.add (Magnet({'pos': (w/2,     h*2/3), 'color': 'white', 'num': n}))
    k.particles.add (Magnet({'pos': (w*9/24,  h/2),   'color': 'blue',  'num': n}))
    k.particles.add (Magnet({'pos': (w*15/24, h/2),   'color': 'blue',  'num': n}))

    # simple player
    pos = vector(k.world.rect.center)
    if k.config.stage > 1:
        pos += (w*3/24, h*3/24)
    k.player.setPos(pos)

    
