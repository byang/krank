
import k
from Part import *

def init():
    k.sound.loadTheme('space')
    k.sound.music()
    k.world.setBackground('level-24')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    k.particles.add(Particle({'pos': (cx, cy), 'color': 'white'}))
    
    # chains
    chains = [(cx, h*1/5),
              (cx, h*4/5),
              (w*1/4, h*1/4),
              (w*1/4, h*3/4),              
              (w*3/4, h*1/4),
              (w*3/4, h*3/4)]
    
    for i in range(len(chains)):
        k.particles.add(Chain({'pos': chains[i], 'color': 'blue'}))

    k.particles.chainCircle((cx, cy), 'orange', 6, h*2/5)

    if k.config.stage > 1:
        chains = [(cx-w*3/8, cy+h*0.2),
                  (cx+w*3/8, cy+h*0.2),
                  (cx-w*3/8, cy-h*0.2),
                  (cx+w*3/8, cy-h*0.2),              
                  (cx-w*3/8, cy+h*0.3),
                  (cx+w*3/8, cy+h*0.3),
                  (cx-w*3/8, cy-h*0.3),
                  (cx+w*3/8, cy-h*0.3),
                  (cx-w*3/8, cy),
                  (cx+w*3/8, cy)]
    
        num_chains = k.config.stage == 2 and 8 or len(chains)
        for i in range(num_chains):
            k.particles.add(Chain({'pos': chains[i], 'color': 'pink'}))
        
    # anchors
    anchor = [(cx, cy-100),
              (cx, cy+100)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'orange', 'maxLinks': 1}))

    anchor = [(cx+100, cy),
              (cx-100, cy)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'blue', 'maxLinks': 1}))

    if k.config.stage > 1: 
        d = k.config.stage == 2 and 130 or 100
        anchor = [(cx+d, cy+d),
                  (cx-d, cy+d),
                  (cx+d, cy-d),
                  (cx-d, cy-d)]
        for i in range(len(anchor)):
            k.particles.add(Anchor({'pos':anchor[i], 'color': 'pink', 'maxLinks': 1}))
        
    # simple player
    k.player.setPos((cx+w*0.2, cy+h*0.1))
