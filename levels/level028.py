
import k
from Part import *

def init():
    k.sound.loadTheme('summer')
    k.sound.music()
    k.world.setBackground('level-28')
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height

    # particles
    parts  = [(cx,  cy+h*2/6),
              (cx,  cy-h*2/6)]
    
    for part in parts:
        k.particles.add(Particle({'pos': part, 'color': 'orange'}))
    
    # chains
    chains = [(cx-w*1/4, cy-h*1/4),
              (cx-w*1/4, cy+h*1/4),              
              (cx+w*1/4, cy-h*1/4),
              (cx+w*1/4, cy+h*1/4),
              (cx-w*1/8, cy-h*1/4),
              (cx-w*1/8, cy+h*1/4),              
              (cx+w*1/8, cy-h*1/4),
              (cx+w*1/8, cy+h*1/4),
              (cx-w*1/4, cy-h*1/8),
              (cx-w*1/4, cy+h*1/8),              
              (cx+w*1/4, cy-h*1/8),
              (cx+w*1/4, cy+h*1/8),
              (cx-w*0.3, cy),
              (cx+w*0.3, cy),
              (cx-w*0.4, cy),
              (cx+w*0.4, cy),
              (cx, cy-h*1/4),
              (cx, cy+h*1/4)]
    
    if k.config.stage == 3:
        chains.extend([(cx-w*0.35, cy-h*1/4),
                       (cx-w*0.35, cy+h*1/4),              
                       (cx+w*0.35, cy-h*1/4),
                       (cx+w*0.35, cy+h*1/4),
                       (cx-w*0.35, cy-h*1/8),
                       (cx-w*0.35, cy+h*1/8),              
                       (cx+w*0.35, cy-h*1/8),
                       (cx+w*0.35, cy+h*1/8),])
    
    for i in range(len(chains)):
        k.particles.add(Chain({'pos': chains[i], 'color': 'white'}))
        
    d = 80
    anchor = [(cx-d, cy-d),
              (cx-d, cy+d), 
              (cx+d, cy-d),
              (cx+d, cy+d),
              (cx, cy),
              (cx-2*d, cy),
              (cx+2*d, cy),]
    num_anchors = [5, 7, 7][k.config.stage-1]
    for i in range(num_anchors):
        maxlinks = i == 4 and 4 or 2
        if k.config.stage == 3 and i < 4:
            maxlinks = 3
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'white', 'maxLinks': maxlinks}))
        
    # simple player
    k.player.setPos((w*0.6,h*0.89))
