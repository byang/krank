
import k
from Part import *

def init():
    k.sound.loadTheme('summer')
    k.sound.music()
    k.world.setBackground('level-06')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
   
    # particles
    num = k.config.stage>=2 and 4 or 2
    k.particles.ballCircle((cx, cy), 'white', num, h*0.32, -math.pi/2)
        
    # chains
    chains = [(  w/4,   h/4),
             (3*w/4,   h/4),
             (  w/4, 3*h/4),
             (3*w/4, 3*h/4)]
    
    if k.config.stage >= 2:
        c = k.config.stage == 2 and 4 or 12
        k.particles.chainCircle((cx, cy), 'white', c, 120, -math.pi/2)
    
    for i in range(len(chains)):
        k.particles.add(Chain({'pos': chains[i], 'color': 'white'}))
    
    # magnets 
    k.particles.add (Magnet({'pos': (cx, cy), 'color': 'white',  'num': num}))
    
    # anchors
    xd, yd = 70, 70
    anchor = [(cx-xd, cy), (cx+xd, cy)]
    if k.config.stage >= 2:
        anchor.extend([(cx, cy-yd), (cx, cy+yd)])
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'white', 'maxLinks': k.config.stage}))
        
    if k.config.stage >= 3:
        k.particles.add(Anchor({'pos':(cx-170, cy),   'color': 'white', 'maxLinks': 1}))
        k.particles.add(Anchor({'pos':(cx+170, cy), 'color': 'white', 'maxLinks': 1}))
        
    # simple player
    k.player.setPos((cx, h/4))
