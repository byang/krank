import k, levels
from Krank  import *
from Part import *

def init():
    k.world.setBackground('menu-levels')
    
    cx, w, h = k.world.rect.centerx, k.world.rect.width, k.world.rect.height
        
    num = min(levels.numLevels-10, k.config.numAvailableLevels()-20)
    pos = []
    cols = 5
    rows = math.ceil(num/cols)

    icons = []
    for i in range(num):
        row, col = math.floor((i%cols))+1.5, math.floor((i/cols))+0.8
        pos.append((row*w/(cols+2), col*h/4.7))
        
        icon = pygame.image.load('levels/unera-icons/level%03d.tga' % (20+i+1))
        if h < 768:
            icon = pygame.transform.scale(icon, (88, 66))
        icons.append(icon)
        
    for i in range(num):
        k.particles.add(Switch({ 'text': '%d' % (20+i+1),
                                 'action': 'k.level.startExit(%d)' % (20+i),
                                 'align': 'bottom',
                                 'size': 'small', 
                                 'offset': icons[i].get_height()*0.6,
                                 'radius': icons[i].get_height()/8,
                                 'image': icons[i],
                                 'pos': pos[i]}))

    k.particles.add(Switch({ 'text': 'Back',
                             'align': 'bottom',
                             'action': 'k.level.menuExit("menu_levels2")',
                             'size': 'small', 
                             'pos': vector((cx, h*0.81))}))
        
    # player
    k.player.setPos(vector((cx, h*0.61)))
    k.player.setTailNum(2, -1)

    k.screen.blit(k.world.image, k.world.rect)    
    pygame.display.update()
    
