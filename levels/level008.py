
import k
from Part import *

def init():
    k.sound.loadTheme('industry')
    k.sound.music()
    k.world.setBackground('level-08')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # particles
    parts = [(  w/4,   h/4),
             (3*w/4,   h/4),
             (  w/4, 3*h/4),
             (3*w/4, 3*h/4)]

    if k.config.stage >= 2:
        parts.extend([(w/4, cy), (3*w/4, cy)])
    
    for i in range(len(parts)):
        k.particles.add(Particle({'pos': parts[i], 'color': 'white'}))
    
    # magnets 
    yd = 190
    n = min(6, 3+k.config.stage)
    k.particles.add (Magnet({'pos': (cx, cy-yd), 'color': 'white', 'num': n-2}))
    k.particles.add (Magnet({'pos': (cx, cy),    'color': 'white', 'num': n}))
    k.particles.add (Magnet({'pos': (cx, cy+yd), 'color': 'white', 'num': n-2}))
    
    # stones
    if k.config.stage >= 2 :
        num = min(k.config.stage*2, 6)
        k.particles.stoneCircle((cx, cy), 'white', num, 80)
        if k.config.stage >= 3:
            k.particles.stoneCircle((cx, cy-yd), 'white', num, 80)
            k.particles.stoneCircle((cx, cy+yd), 'white', num, 80)

    # simple player
    k.player.setPos((cx+(k.config.stage>=2 and 100), 3*h/8))
