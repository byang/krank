
import k
from Part import *

def init():
    k.sound.loadTheme('space')
    k.sound.music()
    k.world.setBackground('level-17')
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height

    k.particles.add(Particle({'pos': (cx, cy), 'color': 'orange'}))

    r1 = [75, 130, 205][k.config.stage-1]
    num_anchors = [4, 8, 6][k.config.stage-1]
    
    k.particles.chainCircle((cx, cy), 'white', num_anchors*2, r1+60)
            
    for i in range(num_anchors):
        pos = vector((cx, cy)) + vector.withAngle(i*2*math.pi/num_anchors, r1)
        k.particles.add(Anchor({'pos': pos, 'color': 'white', 'maxLinks': 2}))
        
    if k.config.stage == 3:
        r2 = 145
        for i in range(6):
            pos = vector((cx, cy)) + vector.withAngle(i*2*math.pi/num_anchors-math.pi/6, r2)
            k.particles.add(Anchor({'pos': pos, 'color': 'white', 'maxLinks': 2}))
        k.particles.chainCircle((cx, cy), 'white', 6, r2)
        k.particles.chainCircle((cx, cy), 'white', 6, r1, -math.pi/6)
    
    # simple player
    o = r1+(k.config.stage<3 and 60)
    k.player.setPos((cx+o, cy+o))
