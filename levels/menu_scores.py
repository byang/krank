import k
from levels import *
from Part import *
from Effect import *
from Tools import *

def init():
    k.sound.loadTheme('menu')
    k.sound.music('summer.ogg')
    k.world.setBackground('menu-scores')

    # simple player
    k.player.setPos(k.world.rect.center)
    k.player.setTailNum(2)
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    drawText("Scores & Stats", (cx, h*2/30), color=(0,0,0))

    lines = [['Total Playing Time', timeString(k.config['totalTime'])]]
    title = ['Easy Levels', 'Hard Levels', 'Extreme Levels', 'Bonus Levels']
    for i in range(3):
        num = i==3 and numBonusLevels or numLevels
        lines.extend([
             0,
             [title[i],        ''],
             ['Time', timeString(k.config['stageTime'][i])],
             ['Solved', "%d/%d (%d)" % (k.config.numSolvedLevels(i+1), num, k.config['totalSolved'][i])]])

    xd = w*0.05
    y = h*6/30
    for i in range(len(lines)):
        if lines[i]:
            y += h/22
            drawText(lines[i][0], (cx+xd, y), size='normal', color=(0,0,0), align='right')
            drawText(lines[i][1], (cx+xd+20, y), size='normal', color=(0,0,0), align='left')
        else:
            y += h/34

    k.particles.add(Switch({ 'text': 'Easy',
                             'align': 'bottom',
                             'action': 'k.level.menuExit("menu_scores1")',
                             'pos': pos(w*1/8, cy-h/4)}))

    k.particles.add(Switch({ 'text': 'Hard',
                             'align': 'bottom',
                             'action': 'k.level.menuExit("menu_scores2")',
                             'pos': pos(w*1/8, cy)}))


    k.particles.add(Switch({ 'text': 'Extreme',
                             'align': 'bottom',
                             'action': 'k.level.menuExit("menu_scores3")',
                             'pos': pos(w*1/8, cy+h/4)}))
        
    k.particles.add(Switch({ 'text': 'Back',
                             'align': 'bottom',
                             'action': 'k.level.menuExit()',
                             'size': 'small', 
                             'pos': pos(cx, h*7/8)}))
    
