import k, levels
from Krank  import *
from Part import *

def init():
    k.sound.loadTheme('menu')
    k.world.setBackground('menu-levels')
    
    cx, w, h = k.world.rect.centerx, k.world.rect.width, k.world.rect.height
        
    num = min(levels.numLevels, k.config.numAvailableLevels())
    max = num
    num = min(10, num)
    pos = []
    cols = 5
    rows = math.ceil(num/cols)

    icons = []
    for i in range(num):
        row, col = math.floor((i%cols))+1.5, math.floor((i/cols))+0.8
        pos.append((row*w/(cols+2), col*h/4.7))
        
        icon = pygame.image.load('levels/unera-icons/level%03d.tga' % (i+1))
        if h < 768:
            icon = pygame.transform.scale(icon, (88, 66))
        icons.append(icon)
        
    for i in range(num):
        k.particles.add(Switch({ 'text': '%d' % (i+1),
                                 'action': 'k.level.startExit(%d)' % i,
                                 'align': 'bottom',
                                 'size': 'small', 
                                 'offset': icons[i].get_height()*0.6,
                                 'radius': icons[i].get_height()/8,
                                 'image': icons[i],
                                 'pos': pos[i]}))

    if num < 16:
        k.particles.add(Switch({ 'text': 'Back',
                                 'align': 'bottom',
                                 'action': 'k.level.menuExit("menu_play")',
                                 'size': 'small', 
                                 'pos': vector((cx-2*w/7, h*0.61))}))

    offset   = 38
    y        = k.world.rect.height >= 1024 and h*0.81 or h*0.79 
    k.particles.add(Switch({ 'text': 'Easy',
                             'align': 'bottom',
                             'action': 'k.config.setStage(1)',
                             'textsize': 'small',
                             'offset': offset,
                             'color': k.config.stage==1 and 'orange' or 'white', 
                             'pos': vector((cx-1*w/7, y))}))

    k.particles.add(Switch({ 'text': 'Hard',
                             'align': 'bottom',
                             'action': 'k.config.setStage(2)',
                             'textsize': 'small', 
                             'offset': offset,
                             'color': k.config.stage==2 and 'orange' or 'white', 
                             'pos': vector((cx, y))}))

    k.particles.add(Switch({ 'text': 'Extreme',
                             'align': 'bottom',
                             'action': 'k.config.setStage(3)',
                             'textsize': 'small', 
                             'offset': offset,
                             'color': k.config.stage==3 and 'orange' or 'white', 
                             'pos': vector((cx+1*w/7, y))}))
    
    if max > 10:
        k.particles.add(Switch({ 'text': 'Next 10',
                                 'align': 'bottom',
                                 'action': 'k.level.menuExit("menu_levels2")',
                                 'size': 'small', 
                                 'pos': vector((cx+2*w/7, h*0.61))}))
    
    # player
    k.player.setPos(vector((cx, h*0.61)))
    k.player.setTailNum(2, -1)

    k.screen.blit(k.world.image, k.world.rect)
    pygame.display.update()
    
