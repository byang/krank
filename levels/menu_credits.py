import k
from levels import *
from Part import *
from Effect import *
from Tools import *

def init():
    k.sound.loadTheme('menu')
    k.sound.music('water.ogg')
    k.world.setBackground('menu-credits')
    # simple player
    k.player.setPos(k.world.rect.center)
    k.player.setTailNum(2)

    index = 0
    
    c, h = k.world.rect.centerx, k.world.rect.height
    drawText('Credits', (c, h*4.0/24.0), color=(0,0,0), valign='center')
    drawText('Code by monsterkodi', (c, h*6/20), color=(0,0,0), size='normal', valign='center')
    drawText('Sound by legoluft', (c, h*7/20), color=(0,0,0), size='normal', valign='center')    
    drawText('Images by Slava Anishenko', (c, h*8/20), color=(0,0,0), size='normal', valign='center')
    drawText('Debianized by Dmitry E. Oboukhov', (c, h*9/20), color=(0,0,0), size='normal', valign='center')
    drawText('Thanks for Nata Karpenko', (c, h*10/20), color=(0,0,0), size='normal', valign='center')
        
    drawText('Version ' + k.VERSION, (c, h*23/20), color=(220,209,180), size='tiny', valign='center')    
        
    k.particles.add(Switch({ 'text': 'Back',
                             'align': 'bottom',
                             'action': 'k.level.menuExit()',
                             'size': 'small', 
                             'pos': vector((c, h*7/8))}))
