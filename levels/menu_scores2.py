import k
from levels import *
from Part import *
from Effect import *
from Tools import *

def init():

    stage = 2
    numLevels = k.config.numAvailableLevels(stage)
    if numLevels > 20:
        k.world.setBackground('menu-scores2')
    else:
        k.world.setBackground('menu-scores2')
    # simple player
    k.player.setPos(k.world.rect.center)
    k.player.setTailNum(2)
    
    c, w, h = k.world.rect.centerx, k.world.rect.width, k.world.rect.height
    
    drawText("Hard Scores", (c, h*2/30), color=(0,0,0))

    for i in range(1, numLevels+1):
        li = i-1
        time = k.config.bestTimeString(i, stage)
        y = numLevels>20 and h*(li%15+5)/25 or h*(li+5)/30
        x = numLevels>20 and (li < 15 and w*0.35 or w*0.62) or c
        lo = numLevels>20 and w/12 or w/6
        level = numLevels>20 and ("%2d" % i) or "Level %2d" % i
        drawText(level, (x-lo, y), size='normal', color=(0,0,0), align='left')
        drawText(time, (x, y), size='normal', color=(0,0,0), align='center')
        drawText(k.config.bestScoreString(i, stage), (x+w/8, y), size='normal', color=(0,0,0), align='right')
        
    k.particles.add(Switch({ 'text': 'Back',
                             'align': 'bottom',
                             'action': 'k.level.menuExit("menu_scores")',
                             'size': 'small', 
                             'pos': pos(c, h*7/8)}))
