import k
from levels import *
from Part import *
from Effect import *

def init():
    k.sound.loadTheme('menu')
    k.sound.music('industry.ogg')
    k.world.setBackground('menu-sounds')
    k.world.darken(60)
    # simple player
    k.player.setPos(k.world.rect.center)
    k.player.setTailNum(2)
    
    c, h = k.world.rect.centerx, k.world.rect.height
    
    drawText('Music            Sound', (c, h*2.0/24.0), valign='center')    
    
    k.particles.add(Switch({ 'text': 'Back', 
                             'align': 'bottom', 
                             'action': 'k.level.menuExit()', 
                             'size': 'small', 
                             'pos': pos(c, h*7/8)}))
    
    for i in range(6):
        dirvec = vector.withAngle(5*math.pi/16-i*math.pi/8, k.world.rect.height*5/12)
        dirvec.y *= 0.8
        k.particles.add(Switch({ 'text': i == 0 and 'Off' or str(i*20), 
                                 'align': 'left', 
                                 'group': 'sound', 
                                 'action': 'k.sound.setSoundVolumeIndex(%d) or k.config.save()' % i, 
                                 'color': i==k.sound.getSoundVolumeIndex() and 'orange' or 'white', 
                                 'pos': vector(k.world.rect.center)+dirvec}))

    for i in range(6):
        dirvec = vector.withAngle(11*math.pi/16+i*math.pi/8, k.world.rect.height*5/12)
        dirvec.y *= 0.8
        k.particles.add(Switch({ 'text': i == 0 and 'Off' or str(i*20), 
                                 'align': 'right', 
                                 'action': 'k.sound.setMusicVolumeIndex(%d) or k.config.save()' % i, 
                                 'group': 'music', 
                                 'color': i==k.sound.getMusicVolumeIndex() and 'orange' or 'white', 
                                 'pos': vector(k.world.rect.center)+dirvec}))    
