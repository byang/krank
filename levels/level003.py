
import k
from Part import *

def init():
    k.sound.loadTheme('industry')
    k.sound.music()
    k.world.setBackground('level-03')
    k.level.linkColor = (0,0,0)

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height

    # chains
    if k.config.stage == 1:
        chains = [(w*1/4, cy), (w*3/4, cy), (cx, cy)]
    elif k.config.stage == 2:
        chains = [(w*1/4, cy-h*1/4), (w*3/4, cy-h*1/4), (cx, cy-h*1/4), (w*1/4, cy+h*1/4), (w*3/4, cy+h*1/4), (cx, cy+h*1/4)]
    else:
        chains = [(w*1/4, cy), (w*3/4, cy), (cx, cy), (w*1/4, cy-h*1/4), (w*3/4, cy-h*1/4), (cx, cy-h*1/4), (w*1/4, cy+h*1/4), (w*3/4, cy+h*1/4), (cx, cy+h*1/4)]
     
    
    for i in range(len(chains)):
        k.particles.add(Chain({'pos': chains[i], 'color': 'orange'}))
        
    # anchors
    dx = 35+k.config.stage*35
    anchor = [(cx-dx, cy),
              (cx+dx, cy)]
    for i in range(len(anchor)):
        k.particles.add(Anchor({'pos':anchor[i], 'color': 'orange', 'maxLinks': 1}))
        
    # stones
    if k.config.stage >= 2:
        num = (k.config.stage-1)*2
        k.particles.stoneCircle((cx-dx, cy), 'orange', num, 70)
        k.particles.stoneCircle((cx+dx, cy), 'orange', num, 70)        
        
    # simple player
    k.player.setPos((cx, h*2/3))
