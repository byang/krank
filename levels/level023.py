
import k
from Part import *

def init():
    k.sound.loadTheme('summer')
    k.sound.music()
    k.world.setBackground('level-23')
    
    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height

    # particle
    k.particles.add(Particle({'pos': (cx, cy), 'color': 'blue'}))
    
    # chains
    chains = [(cx,       cy-h*2/6),
              (cx,       cy+h*2/6),
              (cx,       cy-h*1/6),
              (cx,       cy+h*1/6),
              (cx,       cy-h*1/4),
              (cx,       cy+h*1/4),
              (cx-w*1/4, cy+h*1/4),              
              (cx+w*1/4, cy+h*1/4),
              (cx-w*1/8, cy+h*1/4),              
              (cx+w*1/8, cy+h*1/4),
              (cx-w*3/8, cy+h*1/4),              
              (cx+w*3/8, cy+h*1/4),
              (cx-w*1/4, cy-h*1/4),
              (cx+w*1/4, cy-h*1/4),
              (cx-w*1/8, cy-h*1/4),
              (cx+w*1/8, cy-h*1/4),
              (cx-w*3/8, cy-h*1/4),
              (cx+w*3/8, cy-h*1/4)]
    
    if k.config.stage == 3:
        chains.extend([(cx-w*1/4, cy),              
                      (cx+w*1/4, cy),
                      (cx-w*1/8, cy),              
                      (cx+w*1/8, cy),
                      (cx-w*3/8, cy),              
                      (cx+w*3/8, cy)])
    
    for i in range(len(chains)):
        k.particles.add(Chain({'pos': chains[i], 'color': 'orange'}))
        
    if k.config.stage == 1:
        d = 115
        anchor = [(cx-d, cy), (cx+d, cy)]
        for i in range(len(anchor)):
            k.particles.add(Anchor({'pos':anchor[i], 'color': 'orange', 'maxLinks': 3}))
    else:
        d = 80
        anchor = [(cx-d, cy-d),
                  (cx+d, cy+d),
                  (cx+d, cy-d),
                  (cx-d, cy+d)]        
        
        for i in range(len(anchor)):
            links = k.config.stage == 3 and 4 or (i < 2 and 3 or k.config.stage)
            k.particles.add(Anchor({'pos':anchor[i], 'color': 'orange', 'maxLinks': links}))
        
    #player
    k.player.setPos((w*2/3,h*2/3))
