
import k
from Part import *

def init():
    k.sound.loadTheme('summer')
    k.sound.music()
    k.world.setBackground('level-02')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height
    
    # particles
    parts = [(  w/4,   h/4),
             (3*w/4,   h/4),
             (  w/4, 3*h/4),
             (3*w/4, 3*h/4)]
    
    if k.config.stage >= 3:
        parts.extend([(cx, h/4), (cx, h*3/4)])
    
    for i in range(len(parts)):
        k.particles.add(Particle({'pos': parts[i], 'color': 'white'}))
    
    # magnets 
    n = min(6, k.config.stage*2)
    k.particles.add (Magnet({'pos': (w*1/3, cy), 'color': 'white', 'num': n}))
    k.particles.add (Magnet({'pos': (w*2/3, cy), 'color': 'white', 'num': n}))
    
    # stones
    if k.config.stage >= 2:
        k.particles.stoneCircle((w*1/3, cy), 'white', k.config.stage*2, 80)
        k.particles.stoneCircle((w*2/3, cy), 'white', k.config.stage*2, 80)

    # simple player
    k.player.setPos((cx, cy-120))
