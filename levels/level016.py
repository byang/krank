
import k
from levels import *
from Part import *

def init():
    k.sound.loadTheme('water')
    k.sound.music()
    k.world.setBackground('level-16')

    cx, cy, w, h = k.world.rect.centerx, k.world.rect.centery, k.world.rect.width, k.world.rect.height

    num = k.config.stage+2
    
    # particles    
    k.particles.ballCircle((cx, cy), 'blue',  num, w*0.25,  k.config.stage%2 and  math.pi/2)
    k.particles.ballCircle((cx, cy), 'white', num, w*0.17, k.config.stage%2 and -math.pi/2)
    
    # magnets 
    k.particles.add (Magnet({'pos': (  w/4,   h/4), 'color': 'white', 'num': num}))
    k.particles.add (Magnet({'pos': (3*w/4,   h/4), 'color': 'blue',  'num': num}))
    k.particles.add (Magnet({'pos': (  w/4, 3*h/4), 'color': 'blue',  'num': num}))    
    k.particles.add (Magnet({'pos': (3*w/4, 3*h/4), 'color': 'white', 'num': num}))

    # simple player
    yd = [0, -h/12, 0][k.config.stage-1]
    k.player.setPos((w/2, cy+yd))


    
