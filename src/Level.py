
import k, levels
from Krank import *
from Tools import *

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

class Level:
    
    #-------------------------------------------------------------------------------------------
    def __init__(self, level):
        #log(level, log='startup')
        self.number = isinstance(level, int) and level or 0
        self.nextNumber = 0
        self.exit = False
        self.linkColor = (255,255,255)
        
        names = [os.path.splitext(f)[0] for f in os.listdir('levels') if (f[:5] == 'level' and os.path.splitext(f)[1] == '.py')]
        names.sort()
                                                                               
        self.name = isinstance(level, int) and (level and names[level-1] or "menu") or level

        self.background = None
        k.level = self
        
        k.framed = []
        k.sprites.empty()
        k.particle_sprites.empty()
        k.magnet_sprites.empty()   
        k.effect_sprites.empty()  
        k.player_sprites.empty()
        
        pygame.display.flip()
                
        k.particles.reset()
        k.player.reset()
        k.effects.reset()
        
        self.time = 0
                
        levelModule = None
        try:
            import imp
            file, filename, description = imp.find_module(self.name, levels.__path__)
            levelModule = imp.load_module(self.name, file, filename, description)
            log('level module loaded: %s' % (self.name, ), log='level')
        except Exception, e:
            if e.__class__ <> ImportError:
                error(e)
            log(e)
                
        if levelModule and hasattr(levelModule, 'init'):
            try:
                log('initializing level: %s' % (self.name, ), log='level')
                levelModule.init()
            except Exception, e:
                error(e)
                
        k.score = 0
            
        if k.player.tailnum < 0:
            k.player.setTailNum(k.config.stage*2)
            
        pygame.mouse.set_pos(k.player.pos)
        k.input.targetpos = k.player.pos
        pygame.event.clear()
        k.reset = True
        
        log('level %s initialized' % (self.name, ), log='level')
        
    #-------------------------------------------------------------------------------------------
    def timeString (self, secs=0):
        if secs == 0: 
            secs = self.time/1000
        return timeString(secs)
        
    #-------------------------------------------------------------------------------------------
    def isBonus (self):
        return self.name[:5] == 'bonus'
        
    #-------------------------------------------------------------------------------------------
    def menu (self):
        pygame.time.set_timer(kMENU_LEVEL, 0)
        if self.name[:4] <> 'menu': self.name = 'menu'
        Level(self.name)

    #-------------------------------------------------------------------------------------------
    def menuExit (self, menu_level='menu'):
        if not self.exit:
            self.exit = True
            self.name = menu_level
            log('name', self.name, log='level')
            pygame.time.set_timer(kMENU_LEVEL, 300)
            k.sound.play('exit', force=True)
        return True

    #-------------------------------------------------------------------------------------------
    def startExit (self, level=0):
        self.nextNumber = level+1
        log('nextNumber', self.nextNumber, log='level')
        pygame.time.set_timer(kNEXT_LEVEL, 300)
        k.sound.play('exit', force=True)

    #-------------------------------------------------------------------------------------------
    def restart (self):
        if self.number > 0:
            pygame.display.update(k.world.clearCockpit())
            k.world.resetCockpit()
            k.world.image = None
            Level(self.number)

    #-------------------------------------------------------------------------------------------
    def next (self):
        pygame.time.set_timer(kNEXT_LEVEL, 0)
        if self.number:
            k.sound.reset()
        nextLevel = self.nextNumber or self.number+1
        self.nextNumber = 0
        if nextLevel > k.config.numAvailableLevels():
            nextLevel = 1
        log(nextLevel, log='level')
        Level(nextLevel)
        
    #-------------------------------------------------------------------------------------------
    def load (self, level=None):
        if level == None:
            level = self.number
        Level(level)

    #-------------------------------------------------------------------------------------------
    def previous (self):
        pygame.time.set_timer(kNEXT_LEVEL, 0)
        if self.number:
            k.sound.reset()
        if self.number == 1:
            nextLevel = k.config.lastSolvedLevel()
        else:
            nextLevel = self.number-1
        Level(nextLevel)
        
    #-------------------------------------------------------------------------------------------
    def onFrame(self, delta):
        if not self.exit and not k.world.inTransition():
            self.time += delta
            
    #-------------------------------------------------------------------------------------------
    def checkExit (self):
        if len(k.magnet_sprites) == 0 and len(k.particles.anchors) == 0:
            if not self.exit:
                self.exit = True
                k.config.score(self.number)
                if k.sound.getSoundVolumeIndex() == 0:
                    pygame.time.set_timer(kNEXT_LEVEL, 700)
                else:
                    k.sound.play('exit', force=True, event=kNEXT_LEVEL)
                return True
        return False

    #-------------------------------------------------------------------------------------------
    def saveIcon (self):
        image = pygame.transform.scale(k.screen, (120, 90))
        pygame.image.save(image, 'levels/unera-icons/level%03d.tga' % self.number)
        log('icon saved to ', 'levels/unera-icons/level%03d.tga' % self.number)

    #-------------------------------------------------------------------------------------------
    def saveScreenshot (self):
        file = ('screenshot_%s.tga' % self.name)
        pygame.image.save(k.screen, file)
        log('screenshot saved to', file)
        
    #-------------------------------------------------------------------------------------------
    def paintCockpit (self):

        updates = []
        if self.number > 0 and not k.world.inTransition():

            # ----------------------------------------------------------------------------- time
            
            fadeXOffset, fadeYOffset = 0, 0
            if self.time < 700:
                fac = (1-self.time/700.0)
                fac = fac*fac
                fadeYOffset = -(k.world.rect.height/40+42)*fac
                fadeXOffset = -(k.world.rect.height/30+120)*fac
            
            timeString = k.input.pause and "Pause" or self.timeString()
            textRect = cockpitText(timeString, 
                                    (k.world.rect.width/30, k.world.rect.height/40+fadeYOffset), 
                                    size='small', 
                                    color=(220, 220, 220))
            if k.config.numSolvedLevels() >= k.level.number and k.config.bestTime(k.level.number) < sys.maxint/1000:
                textRect = textRect.union(cockpitText(self.timeString(k.config.bestTime(k.level.number)), 
                                                       (k.world.rect.width/30+fadeXOffset, k.world.rect.height/40+42), 
                                                       size='tiny', 
                                                       color=(180, 180, 180)))
    
            k.world.timerect = textRect
            updates.append(textRect)
            
            # ----------------------------------------------------------------------------- score
    
            textRect = cockpitText("%d" % k.score, 
                                     (k.world.rect.width*29/30, k.world.rect.height/40+fadeYOffset), 
                                     size='small', 
                                     align='right', 
                                     color=(220, 220, 220))
            if k.config.lastSolvedLevel() >= k.level.number and k.config.bestScore(k.level.number) < sys.maxint:
                textRect = textRect.union(cockpitText("%d" % k.config.bestScore(k.level.number), 
                                                         (k.world.rect.width*29/30-fadeXOffset, k.world.rect.height/40+42), 
                                                         size='tiny', 
                                                         align='right', 
                                                         color=(180, 180, 180)))
            k.world.scorerect = textRect
            updates.append(textRect)  
            
        # --------------------------------------------------------------------------------- fps display
            
        if k.debug and 1:
            if k.world.fpsrect:
                k.screen.blit(k.world.image, k.world.fpsrect, k.world.fpsrect)
            fpsrect = drawText("%03d" % k.clock.get_fps(), 
                                 (k.world.rect.width/30, k.world.rect.height*28/30), 
                                 size='small', surface=k.screen, 
                                 align='left', color=(0,0,0))
            k.world.fpsrect = k.world.fpsrect and k.world.fpsrect.union(fpsrect) or fpsrect
            updates.append(k.world.fpsrect)
        
        return updates
        
