#
# Tools.py

from Krank import *
import k

#-----------------------------------------------------------------------------------------------

def quadMirrorSurface (surface, size):
  
  rect = pygame.Rect(0,0,size[0]/2,size[1]/2)
  quad = pygame.transform.scale(surface, rect.size)
  mirror = pygame.Surface(size)
  
  mirror.blit(quad, rect)
  
  rect.left  = rect.width
  quad = pygame.transform.flip(quad, 1, 0)
  mirror.blit(quad, rect)
  
  rect.top = rect.height
  quad = pygame.transform.flip(quad, 0, 1)
  mirror.blit(quad, rect)

  rect.left = 0
  quad = pygame.transform.flip(quad, 1, 0)
  mirror.blit(quad, rect)

  return mirror
  
#-----------------------------------------------------------------------------------------------
def timeString (secs):
    mins = secs/60
    hour = mins/60
    return "%02d:%02d:%02d" % (hour, mins%60, secs%60)

#-----------------------------------------------------------------------------------------------

def textRect (text, pos, align='center', valign='top', color=(255,255,255), size='large'):
    
    txt = k.font[size].render(text, 1, color)
    rect = txt.get_rect().move(pos)

    if align == 'center':
        rect = rect.move(-rect.width/2, 0)
    elif align == 'right':
        rect = rect.move(-rect.width, 0)

    if valign == 'center':
        rect = rect.move(0, -rect.height/2)
    
    return txt, rect

#-----------------------------------------------------------------------------------------------
def drawText (text, pos, align='center', valign='top', color=(255,255,255), size='large', surface=None):
        
    txt, rect = textRect(text, pos, align, valign, color, size)
    if surface is None:
        surface = k.world.image
    return surface.blit(txt, rect)

#-----------------------------------------------------------------------------------------------

images = {}

def image (name):

    if not images.has_key(name):
        file = 'levels/images/' + name + '.png'
        images[name] = pygame.image.load(file)
    return images[name]

#-----------------------------------------------------------------------------------------------
def cockpitText (text, pos, align='left', color=(255,255,255), size='small'):
        
    txt, rect = textRect(text, pos, align, 'top', color, size)

    size = (size == 'small') and 's' or 't'
    img = image('cockpit_'+size+'_l')
    largeRect = pygame.Rect(rect.left-img.get_width(), rect.top-(img.get_height()-rect.height)/2, rect.width+img.get_width()*2, img.get_height())
    leftRect = pygame.Rect(rect.left-img.get_width(), rect.top-(img.get_height()-rect.height)/2, img.get_width(), img.get_height())
    midRect = pygame.Rect(rect.left, rect.top-(img.get_height()-rect.height)/2, rect.width, img.get_height())
    rightRect = pygame.Rect(rect.right, rect.top-(img.get_height()-rect.height)/2, img.get_width(), img.get_height())

    k.screen.blit(img, leftRect)
    img = image('cockpit_'+size+'_m')
    img = pygame.transform.scale(img, (rect.width, largeRect.height))
    k.screen.blit(img, midRect)
    img = image('cockpit_'+size+'_r')
    k.screen.blit(img, rightRect)

    k.screen.blit(txt, rect)

    img = image('cockpit_'+size+'_l_2')
    k.screen.blit(img, leftRect)
    img = image('cockpit_'+size+'_m_2')
    img = pygame.transform.scale(img, (rect.width, largeRect.height))
    k.screen.blit(img, midRect)
    img = image('cockpit_'+size+'_r_2')
    k.screen.blit(img, rightRect)
        
    return largeRect

#-----------------------------------------------------------------------------------------------

def darkenImage (surface, darkness):
    
    blend_size = 32
    blend = pygame.surface.Surface((blend_size, blend_size))
    blend.fill((0,0,0))
    blend.set_alpha(255-255*darkness/100)
    for x in range(surface.get_width()/blend_size+1):
        for y in range(surface.get_height()/blend_size+1):
            surface.blit(blend, (x*blend_size, y*blend_size))

