#
#  Player.py

import k

from Krank import *
from Math   import *
from Part import *
from Input import *

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

class Player:

    #-------------------------------------------------------------------------------------------
    def __init__ (self):
        #log(log='startup')
        k.player = self
        self.reset()
        
    #-------------------------------------------------------------------------------------------        
    def reset (self):
                
        self.pos = pos(0,0)
        self.rot = 0
        
        self.speedFactor = 5
        self.dragFactor  = 0.002
        self.breakFactor = 0.1
        self.maxTargetDistance = 100
        
        self.tailnum = -1
        
    #-------------------------------------------------------------------------------------------        
    def setPos (self, pos):
        self.pos = vector(pos)

    #-------------------------------------------------------------------------------------------        
    def setTailNum (self, tailnum, direction=1):

        self.tailnum = tailnum
        
        self.part = Particle({'pos': self.pos, 'player': 1, 'radius': 20, 
          'image': pygame.image.load('levels/images/circle40.png'), 
          'sprites': k.player_sprites})
        k.particles.add(self.part)
        
        if tailnum:
            img_sizes = [32, 28, 22]
            sizes  = [0] * tailnum
            image  = [0] * tailnum
            radius = [0] * tailnum
                     
            for i in range(tailnum):
                size_index = int(i*float(len(img_sizes))/tailnum)
                sizes[i] = img_sizes[size_index]
                image[i] = pygame.image.load('levels/images/circle%d.png' % sizes[i])
                radius[i] = sizes[i]
    
            op = self.part 
            for i in range(tailnum):
                p = Particle({'pos': op.pos+direction*pos(radius[i],0), 
                              'drag': 0.04, 
                              'player': 2+i, 
                              'radius': radius[i]/2,
                              'image': image[i],
                              'sprites': k.player_sprites})
                k.particles.add(p)
                length = (i > 0) and (sizes[i]/2+sizes[i-1]/2) or (sizes[i]/2+40/2)
                k.particles.add(Spring(op, p, length=length, oneWay=1, spring=300.0, damp=20.0))
                op = p
                
    #-------------------------------------------------------------------------------------------
    def onTick (self, delta):

        try:            
            self.pos = vector(self.part.pos)
                                   
            posToTarget = self.pos.to(k.input.targetpos)
            
            self.part.vel += (posToTarget * delta * self.speedFactor / 1000.0)

            length = self.part.vel.length()
            nearFactor = (1-posToTarget.length()/self.maxTargetDistance)
            dragFactor = 0.002 + 0.09 * nearFactor * nearFactor * nearFactor
            dragFactor *= delta/1000.0
            self.part.vel += -self.part.vel.norm()*length*length*dragFactor
                                   
        except Exception, e:
            error(e)

        