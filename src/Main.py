#
# Main.py

import os, sys

import k

from Krank  import *
from World  import *
from Part   import *
from Level  import *
from Input  import *
from Sound  import *
from Effect import *
from Config import *

k.debug    = 0
startlevel = k.debug and 24
startstage = 1

pygame.init()
pygame.font.init()

Config()

fullscreen = k.config.fullscreen

if k.debug:
    k.config.stage = startstage
    fullscreen = 0

#-------------------------------------------------------------------------------------------

if fullscreen:
    if k.config['screen']:
        width, height = k.config['screen']
    else:
        modes = pygame.display.list_modes(32, pygame.FULLSCREEN)
        modes.sort(reverse=True)
        log(modes, log='startup')
        found = 0
        if (1280, 1024) in modes:
            width, height = (1280, 1024)
            found = 1
        if not found:
            for m in range(len(modes)):
                if modes[m][1] in (1024, 960, 768):
                    width, height = modes[m]
                    found = 1
                    break
        if not found:
            for m in range(len(modes)):
                if modes[m][0] <= 1280:
                    width, height = modes[m]
                    found = 1
                    break                
        if not found:
            width, height = modes[0]
    flags = pygame.FULLSCREEN
else:
	if k.config['screen']:
		width, height = k.config['screen']
	else:
		width, height=800, 600
	flags = pygame.RESIZABLE
    
k.clock = pygame.time.Clock()

#-------------------------------------------------------------------------------------------
# screen 
if os.sys.platform <> 'darwin':
    pygame.display.set_icon(pygame.image.load('levels/images/icon64x64.png'))

#-------------------------------------------------------------------------------------------

World((width, height), flags)

#-------------------------------------------------------------------------------------------
# sprites 

k.sprites          = pygame.sprite.RenderUpdates()
k.player_sprites   = pygame.sprite.RenderUpdates()
k.particle_sprites = pygame.sprite.Group()
k.magnet_sprites   = pygame.sprite.RenderUpdates()
k.effect_sprites   = pygame.sprite.RenderUpdates()

Sound()
Particles()
Effects()
Player()
Input()

k.config.apply()

Level(startlevel)

k.screen.blit(k.world.image, k.screen.get_rect())
pygame.display.flip()

k.clock.tick(0)

#-------------------------------------------------------------------------------------------

def loop():
    while not k.input.exit:
    
        k.reset = False
        k.clock.tick(0)
        
        if not k.input.pause and not k.world.inTransition():
            delta = k.clock.get_time()
    
        updates = []
    
        for spring in k.particles.springs:
            springrect = spring.getRect()
            k.screen.blit(k.world.image, springrect, springrect)
            updates.append(springrect)
        
        k.world.onFrame(delta)
        k.input.onFrame(delta)
        
        updates.append(k.world.clearCockpit())
        
        if not k.input.pause:
            k.level.onFrame(delta)
            k.particles.onFrame(delta)
            if k.reset: 
                continue
            k.effects.onFrame(delta)
    
        k.magnet_sprites.clear(k.screen, k.world.image)
        k.sprites.clear(k.screen, k.world.image)    
        k.player_sprites.clear(k.screen, k.world.image)
                    
        updates.append(k.level.paintCockpit())
        
        if not k.input.pause:
            for magnet in k.particles.magnets: magnet.onFrame(delta)
            for item   in k.framed:              item.onFrame(delta)
            for spring in k.particles.springs: spring.onFrame(delta)
        
        updates.append(k.magnet_sprites.draw(k.screen))
        updates.append(k.sprites.draw(k.screen))
        updates.append(k.player_sprites.draw(k.screen))
        updates.append(k.effect_sprites.draw(k.screen))
    
        for update in updates:
          pygame.display.update(update)
        
if 0:
    import profile, pstats
    profile.run('loop()', 'profile.txt')
    stats = pstats.Stats('profile.txt')
    stats.sort_stats('cumulative')
    stats.print_stats()
else:
    loop()
    
