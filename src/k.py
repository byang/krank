#
# k.py

#-----------------------------------------------------------------------------------------------
# globals

app        = None
config     = None
world      = None
background = None
player     = None
level      = None
particles  = None
screen     = None
magnets    = None
input      = None
sound      = None
effects    = None
clock      = None
reset      = False
framed     = []
debug      = 0
score      = 0
time       = 0
font       = {}

sprites          = None
player_sprites   = None
particle_sprites = None
magnet_sprites   = None
effect_sprites   = None

#-----------------------------------------------------------------------------------------------
# constants

VERSION = "0.7"
KRANK_TITLE = "Krank version %s (Debian revision)"%(VERSION)

print KRANK_TITLE

